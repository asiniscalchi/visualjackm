#include <QtGui/QApplication>
#include <QtCore/QtGlobal>
#include <libprojectM-qt/qprojectm_mainwindow.hpp>
#include <projectM.hpp>
#include "src/visualjackm.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    VisualJackM * mainWindow = new VisualJackM("/usr/share/projectM/config.inp");
    mainWindow->show();

    mainWindow->start();

    return app.exec();
}
