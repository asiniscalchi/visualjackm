include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR})

set(VisualJackM_lib_SRCS visualjackm.cpp)

qt4_automoc(${VisualJackM_lib_SRCS})

add_library(vjm ${VisualJackM_lib_SRCS})
