/*
    VisualJackM let you connect projectM to jack
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "visualjackm.h"

VisualJackM::VisualJackM(QString configFile):
    QProjectM_MainWindow(configFile.toAscii().data(),0)
{
    m_jClient = new QJack::QJackClient("VJM", this);

    if (!m_jClient->open()){
        qWarning() << "VisualJackM | jack server not started";
        exit(EXIT_FAILURE);
    }

    m_port = new QJack::QSignalInPort("in",m_jClient);

    m_jClient->addPort(m_port);

    QObject::connect(m_port, SIGNAL(jackData(QVector<float>)), this,SLOT(setData(QVector<float>)));
}

void VisualJackM::setData(const QVector<float> data)
{

    this->GetProjectM()->pcm()->addPCMfloat(data.data(), data.size());
}

void VisualJackM::start()
{
    m_jClient->activate();
}

VisualJackM::~VisualJackM()
{}

#include "visualjackm.moc"
