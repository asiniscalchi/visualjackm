/*
    VisualJackM let you connect projectM to jack
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef VisualJackM_H
#define VisualJackM_H

#include <QtCore/QtGlobal>
#include <qjack/qjackclient.h>
#include <qjack/qsignalinport.h>
#include <libprojectM-qt/qprojectm_mainwindow.hpp>

class VisualJackM : public QProjectM_MainWindow
{
    Q_OBJECT
public:
    VisualJackM(QString configFile);
    virtual ~VisualJackM();

    void start();
private:
    QJack::QJackClient* m_jClient;
    QJack::QSignalInPort* m_port;

    QVector<float> m_data;

public slots:
    void setData(const QVector<float> data);

};

#endif // VisualJack_H
